# desafio-de-codigo-qa

## Sobre o desafio:

Não existe certo ou errado nesse desafio, a ideia é entender a sua estratégia de testes e suas boas práticas de construção e desenvolvimento.

Você agora faz parte de uma grande empresa, na qual existem vendedores e esses ganham comissões baseadas em suas vendas. Caso a venda seja maior que 10 mil reais, então a comissão será de 6%. Se a venda for menor ou igual a 10 mil reais então a comissão será de 5%.

Lembre-se sempre: A empresa não pode levar prejuízo.

## Tecniquês

- O desafio é em Java.
- O código de negócio já está escrito, então não precisa se preocupar com a implementação.
- Já existe uma classe de testes, pode usá-la a vontade.
- Não é necessário compilar o projeto, apenas rodar os testes.

## O que esperamos como resultado?

- Testes sobre o código de negócio que já existe, utilizando as regras de negócio.
- Cenários os quais você testou.
- Crie uma branch chamada "resultado", para enviar seu código pronto.
- Não precisa automatizar os testes no GitLab.

Boa sorte!
